#!/usr/bin/env python
from __future__ import division
import subprocess
import argparse
import os

import numpy as np
import pandas

def convert_to_plink_input(basename, inputname, chrm, start, stop):
    # TODO: Could apply some filtering here...
    cmd = ['vcftools', '--gzvcf', "{}.vcf.gz".format(inputname),
        '--chr', str(chrm), '--from-bp', str(start), '--to-bp', str(stop), 
        '--plink', 
        '--out', basename]
    process = subprocess.Popen(cmd)
    out, err = process.communicate()
    if out is not None:
        print out
    if err is not None:
        print err
        raise RuntimeError
    os.rename('{}.log'.format(basename), 'vcftools.log')

def generate_LD_map(basename):
    cmd = ['plink', '--file', basename, 
        '--r2', 'square', 'bin', 'single-prec', 
        '--out', basename]
    process = subprocess.Popen(cmd)
    out, err = process.communicate()
    if out is not None:
        print out
    if err is not None:
        print err
        raise RuntimeError
    os.rename('{}.log'.format(basename), 'plink.log')

def find_bin_edges(basename, start, stop, binsize):
    # read in map file
    snps = pandas.read_csv(
        '{}.map'.format(basename),
        header=None, 
        names=['chrom', 'id', '_', 'POS'], 
        delimiter='\t')

    # the rows are snps sorted by bp position
    # group them into genomic bins of equal width
    interval_map = pandas.cut(snps['POS'], range(start, stop+binsize, binsize))
    gby = snps.groupby(interval_map)
    snpcounts = [len(gby.get_group(interval)) for interval in sorted(gby.groups.keys())]
    edges = np.r_[0, np.cumsum(snpcounts)]
    np.savetxt('{}-edges.txt.gz'.format(basename), edges)
    return edges

def enumerate_LD_map(basename):
    # find the row positions that mark the bin edges
    edges = np.loadtxt('{}-edges.txt.gz'.format(basename))
    edge_pairs = zip(edges[:-1], edges[1:])
    total_snps = edges[-1]
    
    # load the binary ld matrix
    mmap = np.memmap('{}.ld.bin'.format(basename), dtype=np.float32)
    mmap.resize(total_snps, total_snps)

    # yield submatrix corresponding to each bin pair
    for i, (i1, i2) in enumerate(edge_pairs):
        for j, (j1, j2) in enumerate(edge_pairs):
            yield (i,j), mmap[i1:i2, j1:j2]

def main(args):
    print "1. Converting vcf file to plink input formats [*.map, *.ped]..."
    convert_to_plink_input(args.out, args.inputname, args.chr, args.start, args.stop)

    print
    print "2. Generating complete LD r2 map [*.ld.bin]..."
    generate_LD_map(args.out)

    print
    print "3. Finding edges for {} kb bins...".format(args.binsize)
    edges = find_bin_edges(args.out, args.start, args.stop, args.binsize)
    num_bins = len(edges) - 1

    print
    print "4. Aggregating and reducing the pairwise r2 data..."
    R2mean = np.zeros((num_bins, num_bins), dtype=np.float32)
    R2medi = np.zeros((num_bins, num_bins), dtype=np.float32)
    R2freq = np.zeros((num_bins, num_bins), dtype=np.float32)
    R2perc = np.zeros((num_bins, num_bins), dtype=np.float32)
    for (i, j), x in enumerate_LD_map(args.out):
        y = x[np.isfinite(x)]
        R2mean[i,j] = np.mean(y)
        R2medi[i,j] = np.median(y)
        R2freq[i,j] = np.sum(y >= args.thresh)/snpcounts[i]/snpcounts[j]
        R2perc[i,j] = np.percentile(y, q=args.percentile)

    print
    print "5. Writing result matrices [*.txt.gz]..."
    outname = '{0}-ldmatrix-{1:.3g}kb'.format(args.out, args.binsize/1000)
    np.savetxt(outname+'-mean.txt.gz', R2mean)
    np.savetxt(outname+'-median.txt.gz', R2medi)
    np.savetxt(outname+'-freq-t{:.3g}.txt.gz'.format(args.thresh), R2freq)
    np.savetxt(outname+'-perc-q{:.3g}.txt.gz'.format(args.percentile), R2perc)

    print "Done!"


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--chr", type=int, required=True)
    parser.add_argument("--start", type=int, required=True)
    parser.add_argument("--stop", type=int, required=True)
    parser.add_argument("--binsize", type=int, required=True)
    parser.add_argument("--out", type=str, required=True, help="basename of output files")
    parser.add_argument("inputname", type=str, help="basename of input vcf file from 1000-genomes")

    parser.add_argument("--thresh", type=int, default=0.1)
    parser.add_argument("--percentile", type=float, default=90)
    
    args = parser.parse_args()
    main(args)
