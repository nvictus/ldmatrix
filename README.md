# ld_r2.py #

Generate and filter an LD r^2 matrix from 1000-genomes data

### Requires ###

* Python 2.7 with numpy, pandas

Make sure these executables are on your path:

* plink-1.9 (still in beta, [homepage](https://www.cog-genomics.org/plink2/))
* vcftools ([homepage](http://vcftools.sourceforge.net/))

### Input ###

A vcf file from [1000 genomes](http://www.1000genomes.org/data#DataAccess) or try the data slicer at:

http://browser.1000genomes.org/Homo_sapiens/UserData/SelectSlice


### Bugs ###